# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/8 下午5:28
# Description :  删除已经上传到oss上的pdf文件
import shutil
from django.conf import settings
from apis.models import PDFOSSModel
from utils.ALIOSS import ALI_OSS

PDF_FILE_ROOT_PATH = settings.PDF_FILE_ROOT_PATH

def delete_pdf(instance):
    """删除pdf文件"""
    if instance.status == 'running':
        return 500
    elif instance.status == 'error':
        pdf_file_path = PDF_FILE_ROOT_PATH.joinpath('output{}'.format(instance.id), 'pdfoutput')
        shutil.rmtree(pdf_file_path.absolute().__str__(), ignore_errors=True)
        return 200
    else:
        oss = ALI_OSS()
        oss_objs = PDFOSSModel.objects.filter(pdf=instance)
        if oss_objs.count()!=0:
            for item in oss_objs:
                name = item.ossName
                oss.delete_file(name)
        return 200
