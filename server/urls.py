import os

from django.contrib import admin
from django.urls import path, include, re_path
from django.views.decorators.csrf import csrf_exempt
from rest_framework import permissions
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi

from server.Healths import Health

openapi_info = openapi.Info(
        title="pdf管理服务",
        default_version='v1',
        description="查看pdf生成状态，下载删除功能",
        # terms_of_service="https://www.tweet.org",
        contact=openapi.Contact(email="wangchen@xinhuotech.com"),
        # license=openapi.License(name="Awesome IP"),
    )
schema_view = get_schema_view(
    openapi_info,
    public=True,
    permission_classes=(permissions.AllowAny,),
)

url_doc_patterns = []
if os.getenv('ENVIRON') != 'test':
    url_doc_patterns = [
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        re_path(r'^docs(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
        # <-- 这里
        path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui')
    ]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('apis.urls')),
    path('health', csrf_exempt(Health.as_view()))
] + url_doc_patterns