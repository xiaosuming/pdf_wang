# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/7 上午11:25
# Description :  返回数据的格式化
def return_mes(code="", data="", mes="SUCCESS"):
    return_dict = {
        "code":code,
        "data":data,
        "msg": mes
    }
    return return_dict
