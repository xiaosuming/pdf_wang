# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/23 下午7:28
# Description :
import re

import requests
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings
from django.http import JsonResponse
HOST = settings.BACKEND
APIS = settings.APIS

class CheckUserMiddleware(MiddlewareMixin):
    def process_request(self, request):

        if white_url(request) == 1:
            return
        token_id = request.headers.get('Authorization')
        url = HOST['CHECK_USER'] + APIS['CHECK_USER']['CHECK_LOGIN']
        headers = {
            "Authorization": token_id
        }
        res = requests.post(url=url, headers=headers)
        if res.status_code == 200:
            data = res.json()
            user_id = data['data']['user_id']
            request.user_id = user_id
        elif res.status_code == 401:
            return JsonResponse(
                status=401,
                data=res.json(),
            )
        else:
            return JsonResponse(
                status=500,
                data={
                    "code":1,
                    "mes":"ERROR",
                    "detail":"验证服务器报错"
                }
            )


def white_url(request):
    # 白名单跳过验证
    white_url_pattern = WHITE_PATTERNS
    url_path = request.get_full_path()
    if url_path == '/docs/' or url_path == '/docs/?format=openapi' or url_path == '/health':
        return 1
    if len(white_url_pattern) == 0:
        return 0
    for pattern in white_url_pattern:
        if pattern['method'] != request.method:
            continue
        res = re.search(pattern['re'], url_path)
        if res:
            return 1
    return 0


WHITE_PATTERNS = [
]