# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/29 下午4:10
# Description :  获取pdf状态
from apis.models import PDFInfoModel
from utils.pdf_status_upload_oss import get_pdf_status
from utils.return_pdf_mes import return_pdf_mes


def pdf_status(pdf_id):
    """pdf状态查询"""
    obj = PDFInfoModel.objects.filter(id=pdf_id)
    if obj.count() == 0:
        print("没有该pdf", pdf_id)
        return {"schedule": -1}
    code = get_pdf_status(pdf_id)
    if code == 100:
        # pdf生成完毕返回pdf信息
        data = return_pdf_mes(obj.first())[0]
        data['schedule'] = 100
    else:
        # 返回pdf生成进度
        data = {"schedule": code}
    return data
