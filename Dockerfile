FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1

RUN sed -i 's@deb.debian.org@mirrors.aliyun.com@g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y  build-essential libssl-dev libffi-dev libgl1-mesa-glx \
    && apt-get install -y python3-dev default-libmysqlclient-dev \
    && apt-get install -y supervisor \
    && rm -r /var/lib/apt/lists/* \
    && mkdir /home/project /tmp/supervisord /home/project/static /home/project/logs


WORKDIR /home/project

COPY ./requirements.txt ./

RUN pip install -r ./requirements.txt -i https://pypi.douban.com/simple/ \
    && rm -rf ~./cache/pip

COPY ./ /home/project

ENTRYPOINT ["/usr/bin/supervisord","-c", "supervisord.conf"]
#ENTRYPOINT ["sh", "start.sh"]