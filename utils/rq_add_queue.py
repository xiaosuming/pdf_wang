# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/30 下午2:57
# Description :  使用rq实现异步功能
import os

import django_rq

def join_queue(func, task_id, *args, timeout=None):
    """
    创建任务队列并把任务加入队列中
    @param timeout: 队列超时时间如果不设定默认为360
    @param task_id: 任务id
    @param func: 所要加入队列的函数
    @param args: 函数所需要的参数
    @return: job_id
    """
    rq_queue_name = 'default'
    # if os.environ.get('CONFIGURATION') != 'test':
    #     queue = django_rq.get_queue(name=rq_queue_name, is_async=False)
    # else:
    #     queue = django_rq.get_queue(name=rq_queue_name)
    queue = django_rq.get_queue(name=rq_queue_name)
    job_id = "job_id_{}".format(task_id)
    rq_id = "rq_id_{}".format(task_id)
    queue.fetch_job(rq_id)
    # 加入任务队列
    queue.enqueue_call(
        func=func,
        args=args,
        timeout=timeout,
        job_id=rq_id,
    )
    return job_id


def get_job_status(task_id):
    rq_id = "rq_id_{}".format(task_id)
    queue = django_rq.get_queue('default')
    rq_job = queue.fetch_job(rq_id)
    if rq_job:
        # 任务完成
        if rq_job.is_finished:
            # 删除该任务
            rq_job.delete()
            # print(1)
            return 'success'
        # 任务创建失败
        elif rq_job.is_failed:
            rq_job.delete()
            # print(2)
            return 'error'
        else:
            # print(3)
            return 'running'
    else:
        # print(4)
        return 'not created'