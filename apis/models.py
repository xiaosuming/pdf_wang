from django.db import models

class PDFInfoModel(models.Model):
    user = models.IntegerField(
        help_text="用户id"
    )
    name = models.CharField(
        max_length=50,
        help_text="pdf名字"
    )
    content = models.CharField(
        max_length=100,
        help_text="pdf导出内容"
    )
    type = models.CharField(
        max_length=20,
        help_text="pdf类型"
    )
    outTime = models.DateTimeField(
        null=True
    )
    volume = models.IntegerField(
        help_text="pdf分测",
        default=0
    )
    people = models.CharField(
        help_text="需要导出的支脉人物",
        max_length=30,
        default=''
    )
    status = models.CharField(
        max_length=10,
        default="running"
    )
    cover = models.CharField(
        max_length=255,
        default=''
    )
    createTime = models.DateTimeField(
        auto_now_add=True
    )
    def __str__(self):
        return "%s" % self.name

    # class Meta:
    #     pass

class PDFOSSModel(models.Model):

    pdf = models.ForeignKey(
        PDFInfoModel,
        on_delete=models.CASCADE,
        help_text="从属于哪个pdf"
    )
    preview = models.BooleanField(
        default=True
    )
    volume = models.CharField(
        help_text="第几册",
        max_length=50
    )
    ossName = models.CharField(
        max_length=45,
        help_text=''
    )
    url = models.CharField(
        help_text="图片地址",
        null=True,
        max_length=255,
    )
    url1 = models.CharField(
        help_text="图片地址",
        null=True,
        max_length=255,
    )
    class Meta:
        unique_together=['pdf', 'preview', 'volume', 'ossName']
