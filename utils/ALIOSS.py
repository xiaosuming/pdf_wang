# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/25 上午11:26
# Description :  与阿里oss交互
import oss2
from itertools import islice
from django.conf import settings
ALI_OSS_CONFIG = settings.ALI_OSS

# ALI_OSS_CONFIG = {
#     "ENDPOINT": "http://oss-cn-shanghai.aliyuncs.com",
#     "AccessKeyId": "LTAI5t8hH2nbNBrZzD1sXAHR",
#     "AccessKeySecret": "AgJfFiQRi3iSLwm1RXB30JpDf8PRNP",
#     "BUCKET": "xinhuo-storage-1256125487"
# }

class ALI_OSS:
    """阿里oss初始化，以及上传、下载、删除、更改"""
    def __init__(self):
        auth = oss2.Auth(access_key_id=ALI_OSS_CONFIG['AccessKeyId'], access_key_secret=ALI_OSS_CONFIG['AccessKeySecret'])
        endpoint = ALI_OSS_CONFIG['ENDPOINT']

        self.bucket = oss2.Bucket(auth=auth, endpoint=endpoint,
                                  bucket_name=ALI_OSS_CONFIG['BUCKET'], connect_timeout=30)


    def upload_file(self, oss_name, file_path):
        """
        文件上传
        Args:
            oss_name:  上传到oss的文件名
            file_path: 文件所在本地绝对路径

        Returns:

        """
        self.bucket.put_object_from_file(key=oss_name, filename=file_path)

    def get_file(self):
        """'etag', 'is_prefix', 'key', 'last_modified', 'owner', 'size', 'storage_class', 'type'"""
        name = []
        for b in islice(oss2.ObjectIterator(self.bucket), 100):
            print(b.key)
            # print(b.type, b.last_modified, b.size, b.storage_class)
            name.append(b.key)
        return name

    def delete_file(self, oss_name):
        """
        删除oss服务器上的文件
        Args:
            oss_name: oss上的文件名

        Returns:

        """
        self.bucket.delete_object(oss_name)

    def download_file(self, oss_name, local_file_path):
        """
        下载到本地目录
        Args:
            oss_name: oss上的文件名
            local_file_path: 本地路径

        Returns:

        """
        self.bucket.put_object_from_file(key=oss_name, filename=local_file_path)

    def create_get_file_url(self, oss_name, time_out):
        """
        创建一个oss上文件的下载临时url
        Args:
            oss_name: oss上的名字
            time_out: url过期时间

        Returns: 文件完整url，可直接访问

        """
        url = self.bucket.sign_url('GET', oss_name, time_out)
        return url

    def create_bucket(self):
        """创建存储空间"""
        bucket_info = self.bucket.get_bucket_info()
        print('name: ' + bucket_info.name)
        print('storage class: ' + bucket_info.storage_class)
        print('creation date: ' + bucket_info.creation_date)
        print('intranet_endpoint: ' + bucket_info.intranet_endpoint)
        print('extranet_endpoint ' + bucket_info.extranet_endpoint)
        print('owner: ' + bucket_info.owner.id)
        print('grant: ' + bucket_info.acl.grant)
        print('data_redundancy_type:' + bucket_info.data_redundancy_type)
        # name = self.bucket.create_bucket(oss2.BUCKET_ACL_PUBLIC_READ,
        #                           oss2.models.BucketCreateConfig(oss2.BUCKET_STORAGE_CLASS_IA))


if __name__ == '__main__':
    upload_name = 'pdf.pdf'
    upload_path = '/media/aaaaa/1.pdf'
    A = ALI_OSS()
    # A.upload_file(upload_name, upload_path)
    # A.delete_file('pdf')
    # names = A.get_file()
    # for i in names:
    #     print(i)
    #     A.delete_file(i)
    # a = A.bucket.sign_url('GET', 'pdf.pdf', 600000)
    # print(a)
    A.create_bucket()