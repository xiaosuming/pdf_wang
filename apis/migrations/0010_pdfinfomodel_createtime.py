# Generated by Django 3.2.4 on 2021-07-01 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0009_alter_pdfinfomodel_people'),
    ]

    operations = [
        migrations.AddField(
            model_name='pdfinfomodel',
            name='createTime',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
