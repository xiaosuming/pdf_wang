from drf_yasg2 import openapi
from drf_yasg2.utils import swagger_auto_schema
from rest_framework import mixins, generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import MultiPartParser, FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.models import PDFInfoModel
from apis.serializers import PDFInfoSerializers
from utils.RabbitMQ import create_pdf_task
from utils.Redis import add_pdf_params
from utils.delete_pdf import delete_pdf
from utils.pdf_oss import pdf_status
from utils.return_mes import return_mes
from utils.return_pdf_mes import return_pdf_mes, return_pdfs_mes

resp_200 = openapi.Response("data参数中的返回信息", PDFInfoSerializers)


class PDFsView(mixins.CreateModelMixin, generics.GenericAPIView):
    """
    get:
    获取所有的pdf信息
    post:
    pdf生成，通过rabbitmq通知到pdf生成服务，由pdf生成服务生成所需要的pdf
    """
    serializer_class = PDFInfoSerializers
    queryset = PDFInfoModel.objects.all()
    @swagger_auto_schema(
        responses={200: PDFInfoSerializers(many=True)}
    )
    def get(self, request, *arg, **kwargs):
        user_id = request.user_id
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=user_id)
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #     serializer = self.get_serializer(page, many=True)
        #     return self.get_paginated_response(serializer.data)
        data = return_pdfs_mes(queryset)
        # serializer = self.get_serializer(queryset, many=True)
        return Response(return_mes(0, {"PDFList":data}))

    # parser_classes = (MultiPartParser,)
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # 想redis中加入pdf所需要的参数
        pdf_data = request.POST.get('pdfData', '{}')
        code = add_pdf_params(serializer.data.get('id'), pdf_data)
        if code == 0:
            return Response(status=200, data=return_mes(1, data='任务已经创建', mes='任务已经创建'))
        # 通过mq创建pdf生成任务
        create_pdf_task(serializer.data.get('id'))
        return Response(return_mes(0, serializer.data), status=status.HTTP_201_CREATED, headers=headers)


class PDFView(generics.GenericAPIView, mixins.UpdateModelMixin):
    """
    get:
    下载单个pdf或在线查看单个pdf状态
    patch:
    修改pdf状态
    delete:
    删除pdf
    """
    serializer_class = PDFInfoSerializers
    queryset = PDFInfoModel
    # @swagger_auto_schema(
    #     manual_parameters=[
    #         openapi.Parameter(
    #             name='download',
    #             in_=openapi.IN_QUERY,
    #             description="下载填true，在线预览填false",
    #             type=openapi.TYPE_BOOLEAN,
    #             default=True
    #         ),
    #         openapi.Parameter(
    #             name='volume',
    #             in_=openapi.IN_QUERY,
    #             description="选择分册",
    #             type=openapi.TYPE_INTEGER,
    #             required=False,
    #             default='1.pdf'
    #         ),
    #     ]
    # )
    # def get(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     volume = request.GET.get('volume')
    #
    #     return

    @swagger_auto_schema(
        responses={200: resp_200}
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        try:
            instance = PDFInfoModel.objects.get(id=kwargs['pk'])
        except PDFInfoModel.DoesNotExist:
            return Response(return_mes(0), status=200)
        code = delete_pdf(instance)
        if code == 500:
            return Response(return_mes(1, data="pdf无法被删除", mes="pdf无法被删除"), status=200)
        else:
            instance.delete()
        return Response(return_mes(0), status=200)


class PDFStatusView(APIView):

    def get(self, request, *args, **kwargs):

        # obj = get_object_or_404(objs, id=kwargs['pk'])
        pdf_id = kwargs['pk']
        data = pdf_status(pdf_id)

        return Response(return_mes(code=0, data=data))