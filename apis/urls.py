# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/8 上午9:33
# Description :
from django.urls import re_path, include, path
from django.views.decorators.csrf import csrf_exempt
from .views import PDFView, PDFsView, PDFStatusView

urlpatterns = [
    path('pdf', csrf_exempt(PDFsView.as_view())),
    path('pdf/<int:pk>', csrf_exempt(PDFView.as_view())),
    path('pdf/<int:pk>/status', csrf_exempt(PDFStatusView.as_view()))
]
