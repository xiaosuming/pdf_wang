from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
from config.ReadConfigJson import Config
CON = Config()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-rc2@hn(dsm^y+70%0_bzuu^t3665ru2kjbsir93+&-*5hs#l(x'

ALLOWED_HOSTS = ['*']

DATABASES = CON.DATABASE
DEBUG = CON.DEBUG
APIS = CON.APIS
BACKEND = CON.BACKEND
ALI_OSS = CON.ALI_OSS
RABBITMQ = CON.RABBITMQ
REDIS = CON.REDIS

# rq配置
RQ_EXCEPTION_HANDLERS= []
RQ_QUEUES = CON.RQ_QUEUES

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'apis.apps.ApisConfig',
    # 'rest_framework_swagger',
    'drf_yasg2',
    'django_rq'
]

# 跨域问题
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    '*',
)

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
    'VIEW',
)

CORS_ALLOW_HEADERS = (
    'Access-Control-Allow-Origin',
    'Sec-Fetch-Mode',
    'XMLHttpRequest',
    'X_FILENAME',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'Pragma'
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'utils.CheckUserMiddleware.CheckUserMiddleware'
]

ROOT_URLCONF = 'server.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'server.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'zh-Hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


STATIC_URL = '/static/'
# STATICFILES_DIRS = [BASE_DIR.joinpath('static').absolute().__str__()]
MEDIA_UEL = '/media/'
MEDIA_ROOT = BASE_DIR.joinpath('media')
STATIC_ROOT = BASE_DIR.joinpath('static')
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# pdf的一些参数
PDF_SCHEDULE = 'pdf_rate_of_process_{}' # pdf进度查询redis的key
PDF_RABBITMQ_KEY = 'family_pdf_task' # pdf rabbitmq的队列名
PDF_REDIS_PARAMS = 'pdf_params_{}'  # 存入redis中pdf所需要的参数
PDF_FILE_ROOT_PATH = MEDIA_ROOT.joinpath('family-task-queue', 'util', 'FamilyTreePDF', 'src')



REST_FRAMEWORK = {
    'UPLOADED_FILES_USE_URL': False,
    'DEFAULT_PAGINATION_CLASS':'rest_framework.pagination.LimitOffsetPagination',
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}

# swagger 配置项
SWAGGER_SETTINGS = {
    # 基础样式
    'SECURITY_DEFINITIONS': {
        "basic":{
            'type': 'basic'
        }
    },
    # 到处文件配置
    'DEFAULT_INFO': 'server.urls.openapi_info',
    # 如果需要登录才能够查看接口文档, 登录的链接使用restframework自带的.
    'LOGIN_URL': 'rest_framework:login',
    'LOGOUT_URL': 'rest_framework:logout',
    "enabled_methods": [
        'get',
        'post',
        'put',
        'patch',
        'delete'
    ],
    # 'DOC_EXPANSION': None,
    # 'SHOW_REQUEST_HEADERS':True,
    # 'USE_SESSION_AUTH': True,
    # 'DOC_EXPANSION': 'list',
    # 接口文档中方法列表以首字母升序排列
    'APIS_SORTER': 'alpha',
    # 如果支持json提交, 则接口文档中包含json输入框
    'JSON_EDITOR': True,
    # 方法列表字母排序
    'OPERATIONS_SORTER': 'alpha',
    'VALIDATOR_URL': None,
}