# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/29 下午7:54
# Description :  返回数据的格式化

from apis.models import PDFOSSModel

def return_pdfs_mes(querysets):
    """列表返回"""
    data = []
    for queryset in querysets:
        d = dict_return(queryset)
        get_oss_url(queryset, data, d)
    return data

def return_pdf_mes(queryset):
    d = dict_return(queryset)
    data = get_oss_url(queryset, None, d, double=True)
    return data

def dict_return(queryset):
    data = {
        "id": queryset.id,
        "name": queryset.name,
        'content': queryset.content,
        'type': queryset.type,
        'outTime': queryset.outTime,
        'volume': queryset.volume,
        'status': queryset.status,
        'people': queryset.people,
        'previewPDF': [],
        'downloadPDF': [],
        'cover': queryset.cover
    }
    return data

def get_oss_url(queryset, data=None, d=None, double=True):
    """获取oss的地址"""
    if d is None:
        d = {}
    if data is None:
        data = []
    if queryset.status == 'ok':
        pdf_objs = PDFOSSModel.objects.filter(pdf=queryset)
        if pdf_objs.count() != 0:
            for item in pdf_objs:
                if item.preview:
                    d['previewPDF'].insert(int(item.volume), item.url)
                else:
                    d['downloadPDF'].insert(int(item.volume), item.url)
    if double is True:
        data.append(d)
        return data
    else:
        return d
