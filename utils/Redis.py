import redis
from django.conf import settings
REDIS, PDF_REDIS_PARAMS = settings.REDIS, settings.PDF_REDIS_PARAMS

class Redis:

    def __init__(self):

        pool = redis.ConnectionPool(host=REDIS['HOST'], port=6379, db=REDIS['DB'],
                                    password=REDIS['PASSWORD'], decode_responses=True)
        self.redis_client = redis.Redis(connection_pool=pool)

    def add_lock(self, key, value, timeout=600):
        """加锁,或存储信息 key = '%s_filesystem_storage' % user.username"""
        self.redis_client.set(name=key, value=value, ex=timeout)

    def add_lock_hash(self, *args, sets=True):
        """存储结构化数据"""
        if sets:
            # 添加redis hash的多个值
            self.redis_client.hmset(*args)
        else:
            # 只添加redis hash的某个值
            self.redis_client.hset(*args)

    def get_value_hash(self, *args, gets=False):
        """
        获取redis中hash类型的值
        :param args:
        :param gets: 是否获取全部参数
        :return:
        """
        if gets:
            value = self.redis_client.hget(*args)
        else:
            value = self.redis_client.hgetall(*args)
        return value

    def get_value(self, key):
        """取值"""
        value = self.redis_client.get(name=key)
        return value

    def check_lock(self, key):
        """
        查看锁是否存在，存在返回True，否则返回False
        @param key: redis的key
        @return:
        """
        return True if self.redis_client.get(key) else False

    def delete_lock(self, key):
        self.redis_client.delete(key)

    def delete_hash_lock(self, *args, delete=True):
        if delete:
            self.redis_client.hdel(*args)
        else:
            self.redis_client.delete(*args)

def add_pdf_params(task_id, data):
    """把pdf所需要的参数传入redis中"""
    key = PDF_REDIS_PARAMS.format(task_id)
    R = Redis()
    value = R.get_value(key)
    if value:
        return 0
    R.add_lock(key, value=data, timeout=36000)
    print('参数已经传入redis中', key, R.get_value(key))
    return 1


