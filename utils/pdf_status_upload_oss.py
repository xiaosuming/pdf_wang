# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/28 下午3:13
# Description :  检查pdf生成状态如果上传成功上传到oss
import shutil
from utils.ALIOSS import ALI_OSS
from utils.Redis import Redis
from apis.models import PDFInfoModel, PDFOSSModel
from django.conf import settings
from datetime import datetime

from utils.rq_add_queue import join_queue, get_job_status

MEDIA, PDF_SCHEDULE, PDF_FILE_ROOT_PATH = settings.MEDIA_ROOT, settings.PDF_SCHEDULE, settings.PDF_FILE_ROOT_PATH

def deal_with_pdf_schedule(value):
    """去除进度中的%号"""
    if value:
        try:
            schedule = int(value)
        except ValueError:
            schedule = int(value.split('%')[0])
        return schedule
    else:
        return -1

def get_pdf_status(pdf_id) -> int:
    """
    获取pdf状态，如果是100%则上传
    Args:
        pdf_id: pdf的id

    Returns:

    """
    key = PDF_SCHEDULE.format(pdf_id)
    R = Redis()
    value = R.get_value(key)
    value = deal_with_pdf_schedule(value)
    # 进度到达60进入异步队列
    if value == 60:
        num = get_file_count(R, pdf_id)
        # 加入异步队列
        join_queue(upload_oss, pdf_id, pdf_id, num, timeout=3600)
        R.add_lock(key, "61", timeout=36000)
        # upload_oss(pdf_id)
        return value
    # 进度大于60开始pdf上传
    elif 100 > value > 60:
        code = get_job_status(pdf_id)
        if code == 'error':
            pdf_obj = PDFInfoModel.objects.get(id=pdf_id)
            # pdf生成传参失败
            pdf_obj.status = 'error'
            pdf_obj.save()
        return value
    # 进度小于60,pdf生成
    elif 0 < value < 60:
        return value
    elif value == 100:
        return 100
    # 进度为-1,pdf生成失败
    else:
        pdf_obj = PDFInfoModel.objects.get(id=pdf_id)
        # pdf生成传参失败
        pdf_obj.status = 'error'
        pdf_obj.save()
        return -1

def upload_oss(pdf_id, num):
    """
    把生成的pdf上传到oss服务器上面
    Args:
        num: 需要上传的文件总数
        pdf_id: pdf的id

    Returns:
    """
    key = PDF_SCHEDULE.format(pdf_id)
    R = Redis()
    pdf_obj = PDFInfoModel.objects.get(id=pdf_id)
    pdf_root_path = PDF_FILE_ROOT_PATH.joinpath('output{}'.format(pdf_id))
    pdf_path = pdf_root_path.joinpath('pdfoutput')
    oss = ALI_OSS()
    i, j = 0, 0
    for item in pdf_path.iterdir():
        print("上传文件", item.name, pdf_path)
        if item.name == 'tmp':
            continue
        oss_name = "{}_{}".format(pdf_id, item.name)
        oss.upload_file(oss_name, item.__str__())
        url = oss.create_get_file_url(oss_name, 3600000000)
        volume = int(item.name[:-4][-1:]) + 1
        if 'preview' in item.name:
            i+=1
            PDFOSSModel.objects.create(pdf=pdf_obj, volume=volume, ossName=oss_name, url=url, preview=True)
        else:
            PDFOSSModel.objects.create(pdf=pdf_obj, volume=volume, ossName=oss_name, url=url, preview=False)
        # 文件删除
        item.unlink()
        if i == num/2:
            # 全部上传数据库修改
            pdf_obj.outTime = datetime.now()
            pdf_obj.volume = i
            pdf_obj.status = 'ok'
            pdf_obj.save()
        # 计算进度
        j += 1
        schedule_int = int((j/int(num)*0.4)*100)+60
        R.add_lock(key, "%d" % schedule_int)
    if i == 0:
        # 没有生成pdf
        pdf_obj.status = 'error'
        pdf_obj.save()
    else:
        # 删除不需要的文件夹
        shutil.rmtree(pdf_root_path.__str__())


def get_file_count(R, pdf_id) -> int:
    """获取文件总数"""
    file_key_num = 'file_key_number_{}'.format(pdf_id)
    value = R.get_value(file_key_num)
    pdf_path = PDF_FILE_ROOT_PATH.joinpath('output{}'.format(pdf_id), 'pdfoutput')
    if value:
        return value
    else:
        num = file_num(pdf_path)
        R.add_lock(file_key_num, num)
    return num

def file_num(pdf_path):
    """计算文件夹中pdf数量"""
    import glob
    files = glob.glob(pathname='{}/*.pdf'.format(pdf_path))
    file_count = len(files)
    return file_count

