# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/28 下午2:28
# Description :  rabbitmq初始化文件
import pika
import json
from django.conf import settings

RABBITMQ, PDF_RABBITMQ_KEY = settings.RABBITMQ, settings.PDF_RABBITMQ_KEY


class RabbitMQ:

    def __int__(self):
        # socket连接
        credentials = pika.PlainCredentials(username=RABBITMQ['USER'], password=RABBITMQ['PASSWORD'])
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ['HOST'], credentials=credentials))
        # 申明一个管道
        self.channel = self.connection.channel()

    def create_queue(self, queue_name):
        """
        给管道里面申明一个queue
        Args:
            queue_name: 队列名

        Returns:

        """
        self.channel.queue_declare(queue=queue_name)

    def send_mes(self, exchange='', routing_key='', body=''):
        """
        发送信息
        Args:
            exchange:
            routing_key:
            body:

        Returns:

        """
        self.channel.basic_publish(
            exchange=exchange,
            routing_key=routing_key,
            body=bytes(body)
        )
        self.connection.close()


def create_pdf_task(taskId):
    """通过mq传递消息，启动pdf生成任务"""
    queue_name = PDF_RABBITMQ_KEY
    body = {"t": taskId}
    body = json.dumps(body)
    print(body, '向rabbitmq的队列中发送消息', queue_name)
    credentials = pika.PlainCredentials(username=RABBITMQ['USER'], password=RABBITMQ['PASSWORD'])
    connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ['HOST'], credentials=credentials))
    channel = connection.channel()
    channel.queue_declare(queue=queue_name, durable=True)
    channel.basic_publish(
        exchange='',
        routing_key=queue_name,
        body=body
    )
    connection.close()
