# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/8 上午9:57
# Description :

from rest_framework import serializers

from utils.Redis import Redis
from .models import PDFInfoModel, PDFOSSModel
from datetime import datetime

class PDFInfoSerializers(serializers.ModelSerializer):
    """pdf信息"""
    name = serializers.CharField(max_length=50, help_text="pdf名字")
    content = serializers.CharField(max_length=100, help_text="pdf导出内容")
    type = serializers.CharField(max_length=20, help_text="pdf类型")
    outTime = serializers.DateTimeField(allow_null=True, required=False)
    # path = serializers.CharField(max_length=255, allow_null=True, required=False)
    volume = serializers.IntegerField(help_text="pdf分测", required=False, default=0)
    # status = serializers.ReadOnlyField(help_text="pdf状态", source="get_status")
    status = serializers.CharField(help_text="pdf状态", max_length=10, required=False)
    people = serializers.CharField(required=False, max_length=30)
    previewPDF = serializers.ReadOnlyField(help_text="url", source="get_preview")
    downloadPDF = serializers.ReadOnlyField(help_text="url", source="get_download")

    # def to_representation(self, instance):
    #
    #     data = super().to_representation(instance)
    #     return data


    def update(self, instance, validated_data):
        instance.outTime = datetime.now()
        instance.name = validated_data.get('name', instance.name)
        instance.content = validated_data.get("content", instance.content)
        instance.type = validated_data.get('type', instance.type)
        instance.path = validated_data.get('path', instance.path)
        instance.volume = validated_data.get('volume', instance.volume)
        instance.save()
        return instance



    class Meta:
        model = PDFInfoModel
        fields = '__all__'






